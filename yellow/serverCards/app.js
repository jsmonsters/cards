const express = require('express');
const app = express();
const path = require("path");
const fs = require("fs");
const multer = require("multer");

app.use(express.static('serverCards'));

app.get("/", express.static(path.join(__dirname, "./public")));

app.use(express.static('public'))

app.listen(3333, () => {
    console.log('Application listening on port 3333! http://localhost:3333/');
});

const upload = multer({
    dest: "c:\\Users\\js\\WebstormProjects\\cards\\yellow\\tasks\\tmp\\"
    // you might also want to set some limits: https://github.com/expressjs/multer#limits
});

app.post(
    "/upload",
    upload.single("file" /* name attribute of <file> element in your form */),

    (req, res) => {
        const tempPath = req.file.path;
        const filename = Date.now() + ".png";
        const targetPath = path.join(__dirname, "./public/uploads/" + filename);
        // path.resolve('/etc', '*')
        console.log('app.post ' + filename);

        if (path.extname(req.file.originalname).toLowerCase() === ".png" ||
            path.extname(req.file.originalname).toLowerCase() === ".jpg") {
            fs.rename(tempPath, targetPath, err => {
                if (err) return handleError(err, res);

                res
                    .status(200)
                    .contentType("text/plain")
                    .end(filename);
            });
        } else {
            fs.unlink(tempPath, err => {
                if (err) return handleError(err, res);

                res
                    .status(403)
                    .contentType("text/plain")
                    .end("Only .png and jpg files are allowed!");
            });
        }
    }
);

