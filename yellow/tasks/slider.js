//todo : upload images using http module
//https://stackoverflow.com/questions/15772394/how-to-upload-display-and-save-images-using-node-js-and-express
var fs = require('fs'),
    request = require('request');

var folder = "images/"
var filename = "image.jpg"

var download = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);
        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};

download('https://cdn.pixabay.com/photo/2020/04/10/21/29/fractals-5027865_960_720.jpg', folder + filename, function () {
    console.log('done');
});

function readFiles(dirname, onFileContent, onError) {
    fs.readdir(dirname, function (err, filenames) {
        if (err) {
            onError(err);
            return;
        }
        filenames.forEach(function (filename) {
            fs.readFile(dirname + filename, 'utf-8', function (err, content) {
                if (err) {
                    onError(err);
                    return;
                }
                onFileContent(filename, content);
            });
        });
    });
}

var data = {};
var str = '';
readFiles(folder, function (filename, content) {
    console.log("filename")
    console.log(filename)
    // data[filename] = content;
    str += "'" + folder + filename + "',";
    fs.writeFile('fileList.js', 'var list = [' + str + ']', () => {
        console.log("fileList created")
    })
}, function (err) {
    throw err;
});

