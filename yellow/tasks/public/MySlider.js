class MySlider {
    //todo : try install in windows MEAN stack

    constructor(params) {
        this.params = params;
        this.intervalId = this.params.intervalId;
        this.sliderId = this.params.sliderId;
        this.data = {};
        this.params.delay = 5;
        this.setNotFound()
        this.setUsers();
        this.setColor();
        this.getImgArr();
    }

    setNotFound(){
        document.getElementById("sliderImg" + this.sliderId).src = "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png";
    }

    setUsers() {
        var str = '';
        for (let i in this.params.users) {
            str += `<a href="?user=${this.params.users[i].pseudo}">${this.params.users[i].name} (${this.params.users[i].role})</a> `
        }

        $('#users' + this.sliderId).html(str);
    }

    setColor() {
        $("#iconColor" + this.sliderId).attr("style", "color:" + this.params.color)
        $("#sliderImgPrev" + this.sliderId).attr("style", "padding: 4px; float: left; height: 32px; width: 50px; background-color:" + this.params.color)
        $("#sliderImgNext" + this.sliderId).attr("style", "padding: 4px; height: 32px; width: 50px; background-color:" + this.params.color)
    }

    setThumbnails(index) {

        if (this.data[index - 1]) {
            $("#sliderImgPrev" + this.sliderId).attr("src", "uploads/" + this.data[index - 1]);
            $("#sliderImgPrev" + this.sliderId).show();
        } else {
            $("#sliderImgPrev" + this.sliderId).hide();
        }

        if (this.data[index + 1]) {
            $("#sliderImgNext" + this.sliderId).attr("src", "uploads/" + this.data[index + 1]);
            $("#sliderImgNext" + this.sliderId).show();
        } else {
            $("#sliderImgNext" + this.sliderId).hide();
        }
    }

    getImgArr() {
        this.getJSON('/images/images.txt',
            function (err, data, that) {
                if (err !== null) {

                } else {
                    let filteredDataArr = [];
                    for (let i in data.Images) {
                        if (data.Images[i].name != '.DS_Store') {
                            const categoryAndNameArr = data.Images[i].name.split("_")
                            const category = categoryAndNameArr[0];
                            if (category == that.sliderId) {
                                filteredDataArr.push(data.Images[i].name);
                            }
                        }
                    }
                    that.data = filteredDataArr;
                    if (that.data.length) {
                        $("#sliderImg" + that.sliderId).attr("src", "uploads/" + that.data[that.data.length - 1]);
                        that.dotsSlide();
                        that.setThumbnails(that.getIndex());
                        //
                    } else {
                        $("#sliderImgPrev" + that.sliderId).hide();
                        $("#sliderImgNext" + that.sliderId).hide();
                    }
                }
            })
    }

    getJSON(url, callback) {
        var that = this;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.onload = function () {
            var status = xhr.status;
            //this.data = xhr.response;
            if (status === 200) {
                callback(null, xhr.response, that);
            } else {
                callback(status, xhr.response, that);
            }
        };
        xhr.send();
    };

    prevImg() {

        var index = this.getIndex();

        var dataLength = this.data.length - 1;
        if (index === 0) {
            document.getElementById("sliderImg" + this.sliderId).src = "uploads/" + this.data[dataLength];
        } else {
            document.getElementById("sliderImg" + this.sliderId).src = "uploads/" + this.data[index - 1];
        }
        this.setThumbnails(index - 1);
        this.setActiveBtn();
    }

    nextImg() {

        var index = this.getIndex();

        if (index !== this.data.length - 1) {
            document.getElementById("sliderImg" + this.sliderId).src = "uploads/" + this.data[index + 1];
        } else {
            document.getElementById("sliderImg" + this.sliderId).src = "uploads/" + this.data[0];
        }
        this.setThumbnails(index + 1);
        this.setActiveBtn();
    }

    getIndex() {
        let img = document.getElementById("sliderImg" + this.sliderId).src.replace("http://localhost:3000/", "")
        return this.data.indexOf(img.replace("uploads/", ""));
    }

    slide() {
        let intervalSlide = setInterval(this.nextImg.bind(this), this.params.delay * 1000);
        document.getElementById(this.sliderId).addEventListener("mouseover", function () {

            clearInterval(intervalSlide);
        });
    }

    play() {
        this.nextImg()
        this.intervalId = setInterval(
            this.nextImg.bind(this), this.params.delay * 1000);
    }

    stop() {
        clearInterval(this.intervalId);
    }

    dotsSlide() {
        var i = 0;
        var index = 0;

        document.getElementById("sliderImg" + this.sliderId + "Ul").innerHTML = '';
        while (i < this.data.length) {

            var li = document.createElement("li");
            li.setAttribute('class', "buttonsLi");
            li.setAttribute('style', "border: 1px solid " + this.params.color);
            if (this.data[i] === document.getElementById("sliderImg" + this.sliderId).src) {
                index = i;
                li.setAttribute('class', "current");
            }
            li.setAttribute('onclick', "slidersArr[" + this.sliderId + "].changeImg(" + i + ")");

            document.getElementById("sliderImg" + this.sliderId + "Ul").appendChild(li);

            i++;
        }
    }

    setActiveBtn() {
        if (this.params.navs === false) {
            return;
        }
        const ul = document.getElementById("sliderImg" + this.sliderId + 'Ul');
        let dotsBtn = [].slice.call(ul.getElementsByTagName("li"));
        for (let key in dotsBtn) {

            dotsBtn[key].classList.remove('current');
            dotsBtn[key].classList.add('buttonsLi');
        }
        var index = this.getIndex();


        if (dotsBtn[index]) {
            dotsBtn[index].classList.remove('buttonsLi');
            dotsBtn[index].classList.add('current');
        }
    }

    changeImg(dotNum) {

        document.getElementById("sliderImg" + this.sliderId).src = "uploads/" + this.data[dotNum];
        var index = this.getIndex();
        this.setThumbnails(index);
        this.setActiveBtn();
    }
}