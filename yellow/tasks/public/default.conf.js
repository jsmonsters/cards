var config = {};
config.category = ["flowers", "animals", "birds", "forest", "flowers2", "animals2", "birds2", "forest2"];
config.colors = ["red", "blue"];
config.colorsRandom = ["darkred", "violet", "green", "red", "magenta", "gray", "yellow", "blue", "purple", "darkslategrey", "brown", "rosybrown", "mediumslateblue", "bisque", "peru", "darkcyan"];
config.users = [{
    name: "Alex",
    pseudo: "user1",
    role: "user",
    categories: ["flowers", "animals"]
},{
    name: "Sofia",
    pseudo: "user2",
    role: "admin",
    categories: ["flowers", "forest", "flowers2", "birds"]
}]