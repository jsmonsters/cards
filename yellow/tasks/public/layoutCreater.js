class LayoutCreator {
    constructor() {
        this.createNav();
    }

    createNav() {
        $('#layout').append(`
        <div class="navbar navbar-expand-md navbar-light sticky-md-top bgrNavbar">
        <br />
        <button class="btn-success btn js-scroll-trigger" id="btnLeftAll"> <i class="fa fa-arrow-circle-left"></i>
        </button>&#160;
        <button class="btn-success btn js-scroll-trigger" id="btnRightAll"> <i class="fa fa-arrow-circle-right"></i>
        </button>&#160;
        <button class="btn-info btn js-scroll-trigger" id="btnPlayAll">Play all <i class="fa fa-play-circle"
                aria-hidden="true"></i></button>&#160;
        <button class="btn-info btn js-scroll-trigger" id="btnStopAll">Stop all <i class="fa fa-pause-circle"
                aria-hidden="true"></i></button>&#160;
    </div>
    <div style="background-color: #e1dff8;" class="content-section container-lg col-md-7">
        <br />
        <h2> <a href="/">All sliders</a> </h2>
        <a href="?c=food">food</a>
        <a href="?c=people">people<br /></a><br />
    </div>`)
        this.addEventNav();

    }

    createLayout(id) {
        $('#layout').append(`<div class="content-section container-lg bg-light col-md-7">
            <details>
            <summary>
            <div>
            <h2 style="background-color: white;">
            <i id="iconColor${id}" class="fa fa-image" aria-hidden="true"></i> ${config.category[id]} <span
        id="users${id}"></span>
    </h2>
    </div>
    </summary>
        <section class="content-section bg-transparent text-center container">
            <div id="slider${id}" style="background-color: #d1cfec; padding: 20px">
                <div style="padding: 1em">
                    <div class="row">
                        <div class="col-sm-2 col-md-2 col-lg-2">&nbsp;</div>
                        <div class="col-sm-8 col-md-8 col-lg-8">
                            <div class="row">
                                <div class="col-12" style="border: 1px dotted; height: 90px; text-align: center;">
                                    <form style="padding: 10px;" id="uploadForm${id}"
                                          method="post" enctype="multipart/form-data" action="/upload">
                                        <input type="file" name="file">
                                        <input type="hidden" name="category" value="${id}">
                                        <input style="margin: 5px;" type="submit" class="btn-warning btn js-scroll-trigger" value="Submit">
                                        <span id="status${id}"></span>
                                    </form>
                                </div>

                                <div class="col-md-12" style="height: 10px; text-align: center;">&nbsp;</div>

                                <div class="col-sm-6 col-md-6 col-lg-6" style=" height: 50px; text-align: end;">
                                    <button class="btn-info btn js-scroll-trigger" style="margin: 5px; width: 100px;" id="btnPlay${id}">Play <i
                                        class="fa fa-play-circle" aria-hidden="true"></i></button>
                                </div>
                                <div class="col-sm-6 col-md-6 col-lg-6" style=" height: 50px; text-align: start;">
                                    <button class="btn-info btn js-scroll-trigger" style="margin: 5px; width: 100px;" id="btnStop${id}">Stop <i
                                        class="fa fa-pause-circle" aria-hidden="true"></i></button>
                                </div>

                                <div class="col-md-12" style="height: 20px; text-align: center;">&nbsp;</div>

                                <div class="col-12" style=" height: 300px; text-align: center;">
                                    <img style="max-width: 400px;" id="sliderImg${id}"/>
                                </div>

                                <div class="col-md-12" style="height: 20px; text-align: center;">&nbsp;</div>

                                <div class="col-sm-2 col-md-2 col-lg-2" style="height: 50px; text-align: start;">
                                    <img id="sliderImgPrev${id}"
                                         style="background-color: gray; padding:20px; width: 90px; height: 80px;"/>
                                </div>

                                <div class="col-sm-8 col-md-8 col-lg-8" style="height: 50px; text-align: center;">
                                    <div id="liDots${id}"
                                         style="height: 40px; text-align: center;">
                                        <ul id="sliderImg${id}Ul" style="text-align: center;"></ul>
                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2 col-lg-2" style="height: 50px; text-align: end;">
                                    <img id="sliderImgNext${id}"
                                         style="background-color: gray; padding:20px; width: 90px; height: 80px;"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2 col-lg-2">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </details>
    </div>`);

        this.addEvents(id); //add functions to buttons
    }

    addEventNav() {
        document.getElementById("btnRightAll").addEventListener("click", function () {
            for (let i in slidersArr) {
                if (slidersArr[i].data.length) {
                    slidersArr[i].nextImg();
                }
            }
            console.log("btnRightAll");
        })
        document.getElementById("btnLeftAll").addEventListener("click", function () {
            for (let i in slidersArr) {
                if (slidersArr[i].data.length) {
                    slidersArr[i].prevImg();
                }
            }
            console.log("btnLeftAll");
        })

        document.getElementById("btnPlayAll").addEventListener("click", function () {
            for (let i in slidersArr) {
                if (slidersArr[i].data.length) {
                    slidersArr[i].play();
                }
            }
        })

        document.getElementById("btnStopAll").addEventListener("click", function () {
            for (let i in slidersArr) {
                if (slidersArr[i].data.length) {
                    slidersArr[i].stop();
                }
            }
        })
    }

    addEvents(sliderId) {
        this.sliderId = sliderId;
        let that = this;
        $('#uploadForm' + sliderId).submit(function () {
            $("#status" + sliderId).empty().text("File is uploading...");
            $(this).ajaxSubmit({
                error: function (xhr) {
                    console.log('Error: ' + xhr.status);
                },
                success: function (response) {
                    $("#status" + sliderId).empty().text(response);
                    console.log("!!!!");
                    console.log(response);
                    console.log("that");
                    console.log(sliderId);
                    slidersArr[sliderId].getImgArr();
                    // $("#sliderImg" + that.sliderId).attr("src", "uploads/" + slidersArr[that.sliderId].data[slidersArr[that.sliderId].data.length - 1]);
                }
            });
            return false;
        });

        document.getElementById("sliderImgNext" + sliderId).addEventListener("click", function () {
            slidersArr[sliderId].nextImg()
        })

        document.getElementById("sliderImgPrev" + sliderId).addEventListener("click", function () {
            slidersArr[sliderId].prevImg()
        });

        document.getElementById("btnPlay" + sliderId).addEventListener("click", function () {
            slidersArr[sliderId].play();
        })

        document.getElementById("btnStop" + sliderId).addEventListener("click", function () {
            slidersArr[sliderId].stop();
        })
    }
}