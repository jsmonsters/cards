var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var PORT = 3001;

// REQUIRE MIDDLEWARE
var instantMongoCrud = require('express-mongo-crud'); // require the module
/*
example
{
    "data":  {"name": "Sofia","pseudo": "user2", "role": "user", "categories": ["flowers", "forest", "flowers2", "birds"], "uniqueId":"user", "description": "new user"}
}
*/

mongoose.connect('localhost:27017/js');

var options = { //specify options
    host: `localhost:${PORT}`
}

//USE AS MIDDLEWARE
app.use(bodyParser.json()); // add body parser
app.use(instantMongoCrud(options)); // use as middleware

app.listen(PORT, ()=>{
    console.log(`started http://localhost:${PORT}/apidoc`);
})