/*
*
* 1. Сделай функцию, которая принимает массив любых целых чисел, которая возращает истинну,
* если все элементы четные, если бы хотя бы один элемент не четный, то false. +
*
Сделай функцию, которая принимает массив любых целых чисел,
* которая возращает истинну, если хотя бы один элемент нечетный, если все четные, то false. +
*
Сделай функцию, которая принимает массив любых целых чисел, которая возращает новый массив,
*  где все элементы кратны пяти. ([1,2,5,12,15,21] вернет [5,15]) +
*
Написать функцию, которая принимает массив чисел, например [1,2,3,4,5] и функция возращает среднее арифметическое, (округлить результат до десятых) +
*
Написать функцию, которая принимает массив чисел, например [1,2,3,4,5], и переносит первый элемент
* массива в конец (например можно засунуть первый элемент в конец,
* затем удалить первый элемент), попробуй несколькими способами сделать, если догадаешься +
*
Написать функцию, которая принимает массив сотрудников, каждый сотрудник имеет имя и возраст ([{name: 'Иван', age: 23},...]) и возвращает массив,
* где каждый элемент представляет из себя строку "Имя: Иван, возвраст: 23". -
*
*
*
* */

/****
 * task function 1
 */
console.log("----------------");
console.log("task Function 1");

let arr1 = [1, 4, 6, 3, 7, 11, 17, 25, 34];
let arr2 = [2, 4, 6, 8];
let arr3 = [1, 3, 5, 7];

function arrayNumbers(array) {
    var check = true;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2) {
            check = false;
        }
    }
    return check;
}

console.log(arrayNumbers(arr3));

/****
 * task function 2
 */
console.log("----------------");
console.log("task Function 2");

let arr4 = [1, 4, 6, 3, 7, 11, 25, 17, 34, 40];

function arrayNumbers2(array) {
    var newArr = [];
    for (let i = 0; i < array.length; i++) {
        if (!(array[i] % 5)) {
            newArr.push(array[i]);
        }
    }
    if (newArr.length) {
        return newArr;
    } else {
        return '%5 not found'
    }
}

console.log(arrayNumbers2(arr4));

/****
 * task function 3
 * *
 Написать функцию, которая принимает массив чисел, например [1,2,3,4,5] и
 функция возращает среднее арифметическое, (округлить результат до десятых) +
 */
console.log("----------------");
console.log("task Function 3");

let arrAverage = [1, 2, 3, 4, 5, 22.5];
let sum = 0;

function calcAverage(arr) {
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return Math.floor(sum / arr.length);
}

console.log(calcAverage(arrAverage));

/****
 * task function 4
 * Написать функцию, которая принимает массив чисел, например [1,2,3,4,5], и переносит первый элемент
 * массива в конец (например можно засунуть первый элемент в конец,
 * затем удалить первый элемент), попробуй несколькими способами сделать, если догадаешься +

 */
console.log("----------------");
console.log("task Function 4");

let newArr = [1, 2, 3, 4, 5];

//version 1
function change() {
    newArr.push(newArr[0]);
    newArr.shift();
    return newArr;
}

console.log(change());

/****
 * task function 5
 * Написать функцию, которая принимает массив сотрудников,
 * каждый сотрудник имеет имя и возраст ([{name: 'Иван', age: 23},...]) и возвращает массив,
 * где каждый элемент представляет из себя строку "Имя: Иван, возвраст: 23". -
 */
console.log("----------------");
console.log("task Function 5");

let workersArr = [
    {name: 'Иван', age: 23},
    {name: 'София', age: 22},
    {name: 'Василий', age: 26},
    {
        name: 'Анна', age: 37
    }];

function workers(workersArr) {
    console.log();
    var arrForReturn = [];
    var str;
    for (var i = 0; i < workersArr.length; i++) {
        str = "Имя: " + workersArr[i].name + ", возраст: " + workersArr[i].age;
        // return "Имя " + workersArr[i].name + ", возраст " + workersArr[i].age;
        arrForReturn.push(str);
    }
    return arrForReturn;
}


console.log(workers(workersArr))

/*
6) У нас есть объект, в котором хранятся зарплаты нашей команды:

let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}
Напишите код для суммирования всех зарплат и сохраните результат в переменной sum. Должно получиться 390.
Если объект salaries пуст, то результат должен быть 0. +

7) Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства объекта obj на 2.

Например:

// до вызова функции
let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

multiplyNumeric(menu);

// после вызова функции
menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
Обратите внимание, что multiplyNumeric не нужно ничего возвращать. Следует напрямую изменять объект.
P.S. Используйте typeof для проверки, что значение свойства числовое. -

8) Написать объект ladder - объект, который позволяет подниматься вверх и спускаться. Пример работы должен быть таким: +

ladder.showStep(); // 0 (выводит ступеньку на который мы находимся)
ladder.up();
ladder.up();
ladder.showStep(); // 2
ladder.down();
ladder.showStep(); // 1
 */
/****
 * task function 6
 * У нас есть объект, в котором хранятся зарплаты нашей команды:

 let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}
 Напишите код для суммирования всех зарплат и сохраните результат в переменной sum. Должно получиться 390.
 Если объект salaries пуст, то результат должен быть 0.
 */
console.log("----------------");
console.log("task Function 6");

let salaries = {
    John: 100,
    Ann: 160,
    Pete: 130
}
//let result = Object.values(salaries);
let summ = 0;

/*
function calcSalary() {
    if (result != null) {
        for (let i = 0; i < result.length; i++) {
            summ += result[i];
        }
        return summ;
    } else {
        return 0;
    }
}
*/
function calcSalary() {
    let result = Object.values(salaries);
    if (result.length) {
        for (var key in salaries) {
            summ += salaries[key];
        }

    }
}

calcSalary()
console.log(summ);

/****
 * task function 7
 * Создайте функцию multiplyNumeric(obj), которая умножает все числовые свойства объекта obj на 2.

 Например:

 // до вызова функции
 let menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

 multiplyNumeric(menu);

 // после вызова функции
 menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
 Обратите внимание, что multiplyNumeric не нужно ничего возвращать. Следует напрямую изменять объект.
 P.S. Используйте typeof для проверки, что значение свойства числовое. -
 */
console.log("----------------");
console.log("task Function 7 version 1");

let menu = {
    width: 200,
    height: 300,
    title: "My menu"
};
let objKeys = Object.keys(menu);
let objValues = Object.values(menu);

let mult = 2;

function multiplyNumeric(obj) {
    for (var i = 0; i < objValues.length; i++) {
        if (typeof objValues[i] == 'number') {
            obj[objKeys[i]] = objValues[i] * mult;
        }
    }
    return obj;
}

var newMenu = multiplyNumeric(menu)

console.log(newMenu);

console.log("----------------");
console.log("task Function 7 version 2");

menu = {
    width: 200,
    height: 300,
    title: "My menu"
};

for (var key in menu) {
    console.log(key + " " + menu[key])
    if (typeof menu[key] == "number") {
        menu[key] = menu[key] * 2;
    }
    //alert("User " + data[key] + " is #" + key); // "User john is #234"
}
console.log("++++++++++new menu+++++++++")
console.log(menu);

/****
 * task function 8
 * Написать объект ladder - объект, который позволяет подниматься вверх и спускаться. Пример работы должен быть таким: +

 ladder.showStep(); // 0 (выводит ступеньку на который мы находимся)
 ladder.up();
 ladder.up();
 ladder.showStep(); // 2
 ladder.down();
 ladder.showStep(); // 1
 */

console.log("----------------");
console.log("task Function 8");

let ladder = {
    step: 0,
    up() {
        return this.step += 1;
    },
    down() {
        return this.step -= 1;
    },
    showStep() {
        return this.step;
    },
};

/*let loader = {};
loader.up = function (){}*/

console.log(ladder.showStep());
console.log(ladder.up());
console.log(ladder.up());
console.log(ladder.showStep());
console.log(ladder.down());
console.log(ladder.showStep());

