module.exports = function(mongoose){
  return [{
    name: {type: String, required: true},
    description: {type: String},
    uniqueId: { type: String, required: true, index: true, unique: true},
    appCategoriesDetails: {
      id: {type: mongoose.Schema.Types.ObjectId}
    }
  }, {
    timestamps: true,
    createdby: false,
    updatedby: true
  }]
};