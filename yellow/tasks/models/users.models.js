module.exports = function(mongoose){
  return [{
    name: {type: String, required: true},
    role: {type: String, required: true},
    pseudo: {type: String, required: true},
    description: {type: String},
    categories: {type: Array, required: true},
    uniqueId: { type: String, required: true, index: true, unique: true},
    appUserDetails: {
      id: {type: mongoose.Schema.Types.ObjectId}
    }
  }, {
    timestamps: true,
    createdby: true,
    updatedby: true
  }]
};