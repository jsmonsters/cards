const http = require("http");
const path = require("path");
const fs = require("fs");

const multer = require("multer");

const express = require("express");

const app = express();
const httpServer = http.createServer(app);

const PORT = process.env.PORT || 3000;

const env = require('dotenv').config()

httpServer.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT} http://localhost:${PORT}`);
});

app.get('/images/images.txt', function (req, res) {
    console.log(env);
    const testFolder = env.parsed.FILE_PATH + "/public/uploads/";
    console.log("testFolder", testFolder);
    // const testFolder = "/Users/sonyk/WebstormProjects/cards/yellow/tasks/public/uploads";
    // const testFolder = "/Users/macspro/cards/yellow/tasks/public/uploads";
    let str = '{"Images":[';
    fs.readdir(testFolder, (err, files) => {
        if(!files.length){
            str += ']}';
        }else{
            files.forEach(file => {
                str += '{"name":"'+file+'"},';
            });
            str = str.slice(0, -1);
            str += ']}';
        }
        res.send(str)
    });
})

const handleError = (err, res) => {
    res
        .status(500)
        .contentType("text/plain")
        .end("Oops! Something went wrong!");
};


const upload = multer({
    dest: env["FILE_PATH"] + "/tmp/"
    // dest: "/Users/Administrator/WebstormProjects/cards/yellow/tasks/tmp/"
    // dest: "/Users/sonyk/WebstormProjects/cards/yellow/tasks/tmp/"
    // dest: "/Users/macspro/cards/yellow/tasks/tmp"
    // you might also want to set some limits: https://github.com/expressjs/multer#limits
});

app.post(
    "/upload",
    upload.single("file" /* name attribute of <file> element in your form */),
    (req, res) => {
        console.log(req.body.category);
        const tempPath = req.file.path;
        const targetPath = path.join(__dirname, "./public/uploads/" + req.body.category + "_" + Date.now() + ".png");

        if (path.extname(req.file.originalname).toLowerCase() === ".png" ||
            path.extname(req.file.originalname).toLowerCase() === ".jpg") {
            fs.rename(tempPath, targetPath, err => {
                if (err) return handleError(err, res);

                res
                    .status(200)
                    .contentType("text/plain")
                    .end("File uploaded!");
            });
        } else {
            fs.unlink(tempPath, err => {
                if (err) return handleError(err, res);

                res
                    .status(403)
                    .contentType("text/plain")
                    .end("Only .png files are allowed!");
            });
        }
    }
);

app.get("/", express.static(path.join(__dirname, "./public")));

app.use(express.static('public'))

