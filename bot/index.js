const TelegramBot = require('node-telegram-bot-api');
//todo : create telegram YOUR_TELEGRAM_BOT_TOKEN
//https://botcreators.ru/blog/botfather-instrukciya/

// replace the value below with the Telegram token you receive from @BotFather
const token = '2144098369:AAF3BnQHBRUVUVGBxm5NSONouaUExuXgiQk';

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

const start = () => {
    bot.setMyCommands([
        {command: '/start', description: 'Приветствие'},
        {command: '/info', description: 'Информация о пользователе'},
    ])

// Matches "/echo [whatever]"
    bot.onText(/\/echo (.+)/, (msg, match) => {
        // 'msg' is the received Message from Telegram
        // 'match' is the result of executing the regexp above on the text content
        // of the message

        const chatId = msg.chat.id;
        const resp = match[1]; // the captured "whatever"

        // send back the matched "whatever" to the chat
        bot.sendMessage(chatId, resp);
    });

// Listen for any kind of message. There are different kinds of
// messages.
    bot.on('message', async msg => {
        const text = msg.text;
        const chatId = msg.chat.id;
        // send a message to the chat acknowledging receipt of their message
        if (text === "/start") {
            return bot.sendSticker(chatId, "https://tlgrm.ru/_/stickers/fdb/2c3/fdb2c3d5-ae19-3b60-8ffc-7b3b8099cfe5/44.webp");
        }
        if (text === "привет" || text === "Привет") {
            return bot.sendMessage(chatId, "Привет!");
        }
        if (text === "/info") {
            return bot.sendMessage(chatId, `Тебя зовут ${msg.from.first_name}`);
        }
        return bot.sendMessage(chatId, `Ты написал мне ${text}`)
    });
}

start();